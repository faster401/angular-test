/* global moment:false */
(function() {
  'use strict';

  angular
    .module('test')
    .constant('moment', moment);

})();
