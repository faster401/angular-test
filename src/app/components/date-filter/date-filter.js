(function() {
  'use strict';

  angular
    .module('test')
    .filter('flightdate', function() {
        return function(input) {
            input = input || '';
            var date = moment(input)
            return date.format("DD.MM.YYYY HH:mm");
        };
    });

})();
