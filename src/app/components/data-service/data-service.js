(function() {
  'use strict';

    angular
        .module('test')
        .factory('dataService', dataService);

    /** @ngInject */
    function dataService($http, $rootScope) {
        var factory = {};

        factory.getData = function() {
            return $http.get($rootScope.api_url + '/en/api/2/forms/flight-booking-selector/').then(function(data) {
                return data;
            }, function(error) {
                return error;
            })
        }

        factory.getCheapFlights = function(form) {
            return $http.get($rootScope.api_url + '/en/api/2/flights/from/' + form.from.id + '/to/' + form.to.id + '/' + form.from_date + '/' + form.to_date + '/250/unique/?limit=15&offset-0').then(function(data) {
                return data;
            }, function(error) {
                return error;
            })  
        }

        return factory;
    };
})();