(function() {
  'use strict';

    angular
        .module('test')
        .directive('flightsTable', flightsTable);

    /** @ngInject */
    function flightsTable() {
        return {
            restrict  : 'E',
            scope: true,
            templateUrl: 'app/components/table-flights/table-flights.html',
        };
    };
})();