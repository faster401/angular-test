(function() {
  'use strict';

    angular
        .module('test')
        .run(runBlock);

    /** @ngInject */
    function runBlock($rootScope) {
        $rootScope.api_url = 'https://murmuring-ocean-10826.herokuapp.com';
    }

})();
