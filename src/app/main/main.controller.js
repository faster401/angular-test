(function() {
    'use strict';

    angular
        .module('test')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController(getData, dataService) {
        
        if(!getData.data)
            return

        var vm = this;
        vm.data = getData.data;
        vm.form = {};

        var mapping = {};
        vm.data.airports.forEach(function(value) {
           mapping[value.iataCode] = value.name;
        });
        
        vm.airports_list = []
        angular.forEach(vm.data.routes, function(value, key) {
            var origin = {id: key, name: mapping[key]}
            vm.airports_list.push(origin);
        });

        vm.findFlights = function() {
            dataService.getCheapFlights(vm.form).then(function(response) {
                vm.result = {};   
                vm.result.flights_list = response.data.flights;
                vm.result.departure_airport = vm.form.from.name;
                vm.result.destination_airport = vm.form.to.name;
            }, function(){
                vm.result = {};
            });
        }
    }

})();
